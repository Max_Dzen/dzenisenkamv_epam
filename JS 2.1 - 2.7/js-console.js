﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function clearInputData() {
	const myNode = document.getElementById("inputData");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
}

function clearConsole() {
	const myNode1 = document.querySelector("#console p");
	while (myNode1.firstChild) {
		myNode1.removeChild(myNode1.firstChild);
	}
}

function task1() {

	const btnTask1 = document.getElementById('btnTask1');

	btnTask1.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const btnExecute = document.createElement("button");

		btnExecute.innerHTML = 'Execute';
		formInputData.appendChild(btnExecute);

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = [];

			for (let i = 0; i < 20; i++) {
				arr.push(i * 5);
			}

			arr.forEach((elem) => {
				jsConsole.writeLine(elem);
			})
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Написать скрипт, который выделяет массив из 20 
							целых чисел и инициализирует каждый элемент по его индексу, умноженному на 5. 
							Выведите полученный массив на консоль.`;
	}
}

task1();

function task2() {

	const btnTask2 = document.getElementById('btnTask2');

	btnTask2.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const label2 = document.createElement("label");
		const input1 = document.createElement("input");
		const input2 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'String 1:';
		label2.innerHTML = 'String 2:';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '81px';

		formInputData.appendChild(label1);
		formInputData.appendChild(label2);
		label1.appendChild(input1);
		label2.appendChild(input2);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		input2.setAttribute('type', 'text');
		input2.setAttribute('id', 'input2');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const string1 = jsConsole.read("#input1").split('');
			const string2 = jsConsole.read("#input2").split('');

			if (string1 < string2) {
				jsConsole.writeLine("The first array is smaller.");
			} else if (string1 > string2) {
				jsConsole.writeLine("The second array is smaller.");

			} else {
				jsConsole.writeLine("The first and second array are equal.");
			}

		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите скрипт, который сравнивает два char массива лексикографически (буква за буквой).`;
	}
}

task2();

function task3() {

	const btnTask3 = document.getElementById('btnTask3');

	btnTask3.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Sequence [, ]';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '121px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = jsConsole.read("#input1").split(',');

			let newArr = arr.reduce(function (a, c) {
				if (a.length && a[a.length - 1][0] === c) {
					a[a.length - 1].push(c);
				} else {
					a.push([c]);
				}
				return a;
			}, []).reduce(function (a, c) {
				return c.length > a.length ? c : a;
			});

			jsConsole.writeLine("The input sequence is:");
			jsConsole.writeLine(arr);
			jsConsole.writeLine("The maximum sequence is:");
			jsConsole.writeLine(newArr);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите скрипт, который найдет максимальную последовательность равных элементов в массиве.`;
	}
}

task3();

function task4() {

	const btnTask4 = document.getElementById('btnTask4');

	btnTask4.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Sequence [, ]';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '121px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = jsConsole.read("#input1").split(',').map((elem) => +elem);

			let newArr = arr.reduce(function (a, c) {
				if (a.length && a[a.length - 1][a[a.length - 1].length - 1] < c) {
					a[a.length - 1].push(c);
				} else {
					a.push([c]);
				}
				return a;
			}, []).reduce(function (a, c) {
				return c.length > a.length ? c : a;
			});

			jsConsole.writeLine("The input sequence is:");
			jsConsole.writeLine(arr);
			jsConsole.writeLine("The maximum increasing sequence is:");
			jsConsole.writeLine(newArr);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите скрипт, который находит максимальную возрастающую последовательность в массиве.`;
	}
}

task4();

function task5() {

	const btnTask5 = document.getElementById('btnTask5');

	btnTask5.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Sequence [, ]';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '121px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = jsConsole.read("#input1").split(',').map((elem) => +elem);

			const selectionSort = arr => {
				for (let i = 0, l = arr.length, k = l - 1; i < k; i++) {
					let indexMin = i;
					for (let j = i + 1; j < l; j++) {
						if (arr[indexMin] > arr[j]) {
							indexMin = j;
						}
					}
					if (indexMin !== i) {
						[arr[i], arr[indexMin]] = [arr[indexMin], arr[i]];
					}
				}
				return arr;
			};

			const arrSort = selectionSort(arr);

			arrSort.forEach((elem, index) => {
				jsConsole.writeLine(`${index}: ${elem}`);
			})
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Отсортировать массив – значит расположить его элементы в порядке возрастания. 
		Напишите сценарий для сортировки массива. Используйте selection sort алгоритм: найти наименьший элемент, 
		переместить его на первое место, найти наименьшее из остальных, переместите его на вторую позицию и т. д.`;
	}
}

task5();

function task6() {

	const btnTask6 = document.getElementById('btnTask6');

	btnTask6.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Sequence [, ]';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '121px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = jsConsole.read("#input1").split(',').map((elem) => +elem);

			const selectionSort = arr => {
				for (let i = 0, l = arr.length, k = l - 1; i < k; i++) {
					let indexMin = i;
					for (let j = i + 1; j < l; j++) {
						if (arr[indexMin] > arr[j]) {
							indexMin = j;
						}
					}
					if (indexMin !== i) {
						[arr[i], arr[indexMin]] = [arr[indexMin], arr[i]];
					}
				}
				return arr;
			};

			const arrSort = selectionSort(arr);

			let newArr = arr.reduce(function (a, c) {
				if (a.length && a[a.length - 1][0] === c) {
					a[a.length - 1].push(c);
				} else {
					a.push([c]);
				}
				return a;
			}, []).reduce(function (a, c) {
				return c.length > a.length ? c : a;
			});

			console.log(arr);
			console.log(arrSort);
			console.log(newArr[0], newArr.length);

			jsConsole.writeLine(`${newArr[0]} (${newArr.length} times)`);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите программу, которая найдет наиболее частое число в массиве.`;
	}
}

task6();

function task7() {

	const btnTask7 = document.getElementById('btnTask7');

	btnTask7.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const label2 = document.createElement("label");
		const input1 = document.createElement("input");
		const input2 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Sequence [, ]';
		label2.innerHTML = 'Searchhed Value';
		btnExecute.innerHTML = 'Execute';

		input1.style.marginLeft = '35px';
		btnExecute.style.marginLeft = '145px';

		formInputData.appendChild(label1);
		formInputData.appendChild(label2);
		label1.appendChild(input1);
		label2.appendChild(input2);
		formInputData.appendChild(btnExecute);


		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		input2.setAttribute('type', 'text');
		input2.setAttribute('id', 'input2');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = jsConsole.read("#input1").split(',').map((elem) => +elem);
			const searchhedValue = +jsConsole.read("#input2");

			const selectionSort = arr => {
				for (let i = 0, l = arr.length, k = l - 1; i < k; i++) {
					let indexMin = i;
					for (let j = i + 1; j < l; j++) {
						if (arr[indexMin] > arr[j]) {
							indexMin = j;
						}
					}
					if (indexMin !== i) {
						[arr[i], arr[indexMin]] = [arr[indexMin], arr[i]];
					}
				}
				return arr;
			};

			const arrSort = selectionSort(arr);

			const binarySearch = (value, array) => {
				let first = 0;
				let last = array.length - 1;
				let position = -1;
				let found = false;
				let middle;

				while (found === false && first <= last) {
					middle = Math.floor((first + last) / 2);
					if (array[middle] == value) {
						found = true;
						position = middle;
					} else if (array[middle] > value) {
						last = middle - 1;
					} else {
						first = middle + 1;
					}
				}
				return position;
			}

			const positionValue = binarySearch(searchhedValue, arrSort);

			jsConsole.writeLine('Sorted array:');
			jsConsole.writeLine(arrSort);
			jsConsole.writeLine(`The index of ${searchhedValue} is ${positionValue}`);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите программу, которая находит индекс данного элемента в отсортированном массиве целых чисел,
		 используя алгоритм двоичного поиска.`;
	}
}

task7();