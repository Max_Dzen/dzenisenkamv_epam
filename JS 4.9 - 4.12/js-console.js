﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function clearInputData() {
	const myNode = document.getElementById("inputData");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
}

function clearConsole() {
	const myNode1 = document.querySelector("#console p");
	while (myNode1.firstChild) {
		myNode1.removeChild(myNode1.firstChild);
	}
}

function task1() {

	const btnTask1 = document.getElementById('btnTask1');

	btnTask1.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const btnExecute = document.createElement("button");

		btnExecute.innerHTML = 'Execute';

		formInputData.appendChild(btnExecute);

		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const str = `Impad1945@superrito.com jhekffhe1231@ RArly1960@gustr.de maks@mail.ru lorem ipsum hello@gmail.com Ivan3000@mail.ru world miRskiu@mail.ru`;

			extractAddress(str).forEach((elem) => {
				jsConsole.writeLine(elem);
			});

			function extractAddress(str) {

				const arr = str.match(/\S+@\S+\.\S+/g);

				return arr;
			}
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию для извлечения всех адресов электронной почты из данного текста.`;
	}
}

task1();

function task2() {

	const btnTask2 = document.getElementById('btnTask2');

	btnTask2.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const btnExecute = document.createElement("button");

		btnExecute.innerHTML = 'Execute';

		formInputData.appendChild(btnExecute);

		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = ['ABBA', 'civic', 'kayak', 'reviver', 'radar', 'racecar', 'level', 'rotor', 'redder', 'madam', 'malayalam', 'refer', 'lorem', 'ipsum', 'dolor', 'sit', 'consectetur'];

			function isPalindrome(arr) {

				arr.forEach((elem) => {
					if (elem === elem.split('').reverse().join('')) {
						jsConsole.writeLine(elem);
					}
				});

			}

			isPalindrome(arr);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Написать программу, которая извлекает из заданного текста все палиндромы.`;
	}
}

task2();

function task3() {

	const btnTask3 = document.getElementById('btnTask3');

	btnTask3.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const btnExecute = document.createElement("button");

		btnExecute.innerHTML = 'Execute';

		formInputData.appendChild(btnExecute);

		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const str = stringFormat("The coordinates of the {0} are X: {1} and Y: {2}", "rectangle", 25, 36);

			function stringFormat(str, ...rest) {

				for (k in rest) {
					str = str.replace("{" + k + "}", rest[k])
				}

				return str;
			};

			jsConsole.writeLine(str);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию, которая форматирует строку с помощью заполнителей.`;
	}
}

task3();

function task4() {

	const btnTask4 = document.getElementById('btnTask4');

	btnTask4.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const btnExecute = document.createElement("button");

		btnExecute.innerHTML = 'Execute';

		formInputData.appendChild(btnExecute);

		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const people = [
				{ name: "Шапокляк", age: 55 },
				{ name: "Чебурашка", age: 17 },
				{ name: "Крыска-Лариска", age: 18 },
				{ name: "Крокодильчик", age: 26 },
				{ name: "Турист - завтрак крокодильчика", age: 32 },
			];

			const div = document.createElement("div");
			const strong = document.createElement("strong");
			const span = document.createElement("span");
			const divConsole = document.getElementById("console");

			strong.innerHTML = '-{name}-';
			span.innerHTML = `-{age}-`;

			div.appendChild(strong);
			div.appendChild(span);
			divConsole.appendChild(div);

			div.setAttribute('data-type', 'template');
			div.setAttribute('id', 'list-item');

			const divPeople = document.getElementById("list-item");
			const template = divPeople.innerHTML;
			const peopleList = generateHtmlList(people, template);

			divPeople.innerHTML = peopleList;

			function generateHtmlList(array, template) {
				return "<ul><li>" + array.map(e => template.replace("{name}", e.name).replace("{age}", e.age)).join`</li><li>` + "</li></ul>";
			}
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Написать функцию, которая создает HTML UL, используя шаблон для каждого HTML LI. Источник списка должен быть массивом элементов. Замените все заполнители, помеченные значением соответствующего свойства объекта.`;
	}
}

task4();