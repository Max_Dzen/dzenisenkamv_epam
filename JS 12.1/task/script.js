const requestURL = 'http://localhost/denwer/task/data.json';

const btnGet = document.querySelector('.get');
const btnPost = document.querySelector('.post');
const form = document.querySelector('form');

btnGet.addEventListener('click', dataGet);

function dataGet(ev) {
    ev.preventDefault();

    const getP = document.createElement("p");
    getP.setAttribute('class', 'getP');

    document.body.prepend(getP);

    function sendRequest(method, url, body = null) {
        return fetch(url).then(response => {
            return response.text()
        })
    }

    sendRequest('GET', requestURL)
        .then(data => getP.innerHTML = data)
        .catch(err => console.log(err));
}

btnPost.addEventListener('click', dataPost);

function dataPost(ev) {
    ev.preventDefault();

    const inputFirst = document.createElement("input");
    const labelFirst = document.createElement("label");
    const inputLast = document.createElement("input");
    const labelLast = document.createElement("label");
    const inputAge = document.createElement("input");
    const labelAge = document.createElement("label");
    const btnMakePost = document.createElement("button");
    const getP = document.querySelector('.getP');

    labelFirst.innerHTML = 'First name:';
    labelLast.innerHTML = 'Last name:';
    labelAge.innerHTML = 'Age:';
    btnMakePost.innerHTML = 'Make post request';

    inputFirst.setAttribute('type', 'text');
    inputFirst.setAttribute('id', 'inputFirst');
    inputLast.setAttribute('type', 'text');
    inputLast.setAttribute('id', 'inputLast');
    inputAge.setAttribute('type', 'text');
    inputAge.setAttribute('id', 'inputAge');
    btnMakePost.setAttribute('class', 'btnMakePost');

    form.prepend(btnMakePost);
    form.prepend(labelAge);
    form.prepend(labelLast);
    form.prepend(labelFirst);

    labelFirst.append(inputFirst);
    labelLast.append(inputLast);
    labelAge.append(inputAge);

    btnGet.style.display = 'none';
    btnPost.style.display = 'none';
    getP.style.display = 'none';

    btnMakePost.addEventListener('click', dataMakePost);

    function dataMakePost(ev) {
        ev.preventDefault();

        const inputFirst = document.querySelector("#inputFirst");
        const inputLast = document.querySelector("#inputLast");
        const inputAge = document.querySelector("#inputAge");
        const getP = document.querySelector('.getP');
        const postP = document.createElement('p');

        form.prepend(postP);

        btnGet.style.display = 'block';
        btnPost.style.display = 'block';
        getP.style.display = 'block';

        labelAge.style.display = 'none';
        labelLast.style.display = 'none';
        labelFirst.style.display = 'none';
        btnMakePost.style.display = 'none';

        function sendRequests(method, url, body = null) {
            const headers = {
                'Content-Type': 'application/json'
            }

            return fetch(url, {
                method: method,
                body: JSON.stringify(body),
                headers: headers
            }).then(response => {
                return response.text()
            })
        }

        const body = {
            firstName: inputFirst.value,
            lastName: inputLast.value,
            age: inputAge.value
        }

        sendRequests('POST', requestURL, body)
            .then(data => getP.innerHTML = data)
            .catch(err => console.log(err))

        postP.innerHTML = `{"firstName": "${body.firstName}", "lastName": "${body.lastName}", "age": "${body.age}"}`;
    }
}