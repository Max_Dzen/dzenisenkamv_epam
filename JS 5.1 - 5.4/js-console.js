﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function clearInputData() {
	const myNode = document.getElementById("inputData");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
}

function clearConsole() {
	const myNode1 = document.querySelector("#console p");
	while (myNode1.firstChild) {
		myNode1.removeChild(myNode1.firstChild);
	}
}

function task1() {

	const btnTask1 = document.getElementById('btnTask1');

	btnTask1.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const label2 = document.createElement("label");
		const input2 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'a:';
		label2.innerHTML = 'b:';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '26px';

		formInputData.appendChild(label1);
		formInputData.appendChild(label2);
		label1.appendChild(input1);
		label2.appendChild(input2);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		input2.setAttribute('type', 'text');
		input2.setAttribute('id', 'input2');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const a = jsConsole.read("#input1");
			const b = jsConsole.read("#input2");

			conc(a, b);

			function conc(a, b) {
				return jsConsole.writeLine(a + b);
			}
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Создайте функцию conc, которая должна соединять два параметра a и b и возвращать соединяющую строку с помощью оператора function Declaration (FDS). Вызов этой функции должен происходить перед ее объявлением. Тестовые данные: a =1, b =1, результат = 11.`;
	}
}

task1();

function task2() {

	const btnTask2 = document.getElementById('btnTask2');

	btnTask2.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const label2 = document.createElement("label");
		const input2 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'a:';
		label2.innerHTML = 'b:';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '26px';

		formInputData.appendChild(label1);
		formInputData.appendChild(label2);
		label1.appendChild(input1);
		label2.appendChild(input2);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		input2.setAttribute('type', 'text');
		input2.setAttribute('id', 'input2');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const a = jsConsole.read("#input1");
			const b = jsConsole.read("#input2");

			const comp = function (a, b) {
				const result = (a === b) ? 1 : -1;

				return result;
			};

			jsConsole.writeLine(comp(a, b));
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Создайте функцию comp, которая должна сравнить два параметра a и b и возвратить 1, если a равно b и -1, если a не равно b, используя Function Definition Expression. Тестовые данные: а = abc, b = abc, результат =1, а = abC, b = abc, результат= -1.`;
	}
}

task2();

function task3() {

	const btnTask3 = document.getElementById('btnTask3');

	btnTask3.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const btnExecute = document.createElement("button");

		btnExecute.innerHTML = 'Execute';

		formInputData.appendChild(btnExecute);

		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', (ev) => {
			ev.preventDefault();
			clearConsole();

			return jsConsole.writeLine(`message in console`);

		});

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Создайте анонимную функцию, которая должна вывести сообщение «message in console» в консоли.`;
	}
}

task3();

function task4() {

	const btnTask4 = document.getElementById('btnTask4');

	btnTask4.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Number:';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '81px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const numb = +jsConsole.read("#input1");

			const fib = function calcNumb(numb) {

				let a = 1;
				let b = 1;

				for (let i = 3; i <= numb; i++) {
					let c = a + b;
					a = b;
					b = c;
				}

				return b;
			};

			jsConsole.writeLine(fib(numb));
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Последовательность чисел Фибоначчи вычисляется по формуле формуле F(n) = F(n-1) + F(n-2). В ней каждое следующее число вычисляется как сумма двух предыдущих. Первые два числа равны 1 и 1.
		Напишите функцию fib(n), которая возвращает n-е число Фибоначчи.`;
	}
}

task4();

function task5() {

	const btnTask5 = document.getElementById('btnTask5');

	btnTask5.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const textarea = document.createElement("textarea");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Text:';
		btnExecute.innerHTML = 'Execute';

		textarea.style.marginLeft = '32px';
		btnExecute.style.marginLeft = '72px';

		formInputData.appendChild(label1);
		label1.appendChild(textarea);
		formInputData.appendChild(btnExecute);

		textarea.setAttribute('id', 'textarea');
		textarea.setAttribute('cols', '25');
		textarea.setAttribute('rows', '7');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const text = jsConsole.read("#textarea");

			function checkSpam(text) {

				const str = text.toLowerCase();

				if (str.indexOf("sex") !== -1 || str.indexOf("spam") !== -1) {
					return true;
				} else {
					return false;
				}
			}

			jsConsole.writeLine(checkSpam(text));

		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию checkSpam которая проверяет строку на содержание слов: spam, sex.`;
	}
}

task5();

function task6() {

	const btnTask6 = document.getElementById('btnTask6');

	btnTask6.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const textarea = document.createElement("textarea");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Text:';
		btnExecute.innerHTML = 'Execute';

		textarea.style.marginLeft = '32px';
		btnExecute.style.marginLeft = '72px';

		formInputData.appendChild(label1);
		label1.appendChild(textarea);
		formInputData.appendChild(btnExecute);

		textarea.setAttribute('id', 'textarea');
		textarea.setAttribute('cols', '25');
		textarea.setAttribute('rows', '7');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const text = jsConsole.read("#textarea");

			function lineCut(text) {

				const str = text;

				if (str.length > 20) {
					return `${str.slice(0, 20)}...`;
				} else {
					return str;
				}

			}

			jsConsole.writeLine(lineCut(text));
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию, которая принимает на вход строку и возвращает ее неизменной, если ее длина не превышает 20 символов. Если длина больше 20, то обрезает строку и добавляет в конец строки '...'`;
	}
}

task6();