﻿function task1() {

	const btn1 = document.getElementById('btn1');
	const btn2 = document.getElementById('btn2');

	btn1.addEventListener('click', getElementsByTagName);
	btn2.addEventListener('click', querySelector);

	function getElementsByTagName(ev) {
		ev.preventDefault();

		const divCollection = document.getElementsByTagName('div');
		divCollection[2].style.border = "4px solid red";
		divCollection[3].style.border = "4px solid red";
	}

	function querySelector(ev) {
		ev.preventDefault();
		
		const divCollection = document.querySelectorAll('div');
		divCollection[2].style.border = "4px solid green";
		divCollection[3].style.border = "4px solid green";
	}
}

task1();