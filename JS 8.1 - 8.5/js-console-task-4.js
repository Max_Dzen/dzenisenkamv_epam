﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function clearInputData() {
	const myNode = document.getElementById("inputData");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
}

function task4() {

	const btnTask1 = document.getElementById('btnTask1');

	btnTask1.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();

		const formInputData = document.getElementById('inputData');
		const divConsole = document.getElementById('console');
		const btnCreateDivs = document.createElement("button");
		const btnAnimateDivs = document.createElement("button");
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');

		btnCreateDivs.style.marginLeft = '131px';
		btnAnimateDivs.style.marginLeft = '131px';

		label1.innerHTML = 'Number of div:';
		btnCreateDivs.innerHTML = 'Create Divs';
		btnAnimateDivs.innerHTML = 'Animate Divs';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnCreateDivs);
		formInputData.appendChild(btnAnimateDivs);

		btnCreateDivs.setAttribute('type', 'submit');
		btnAnimateDivs.setAttribute('type', 'submit');

		btnCreateDivs.addEventListener('click', createDivs);

		let startAngle = [];

		function createDivs(ev) {
			ev.preventDefault();

			const numberDiv = +jsConsole.read("#input1");

			for (let i = 0; i < numberDiv; i++) {

				const div = document.createElement("div");
				div.style.width = "20px";
				div.style.height = "20px";
				div.style.borderRadius = "50%";
				div.style.background = "white";
				div.style.position = "absolute";

				const CenterX = 400;
				const CenterY = 450;
				const radius = 150;

				const x = (CenterX + radius * Math.cos(2 * Math.PI * i / numberDiv));
				const y = (CenterY + radius * Math.sin(2 * Math.PI * i / numberDiv));

				startAngle.push(2 * Math.PI * i / numberDiv);

				div.style.left = `${x}px`;
				div.style.top = `${y}px`;

				divConsole.appendChild(div);
			}
		}

		btnAnimateDivs.addEventListener('click', animateDivs);

		function animateDivs(ev) {
			ev.preventDefault();

			const div = document.getElementById('console');

			const CenterX = 400;
			const CenterY = 450;
			const radius = 150;
			let a = 0;

			function moveDiv(a) {

				for (let i = 0; i < div.children.length; i++) {

					let x = Math.floor(CenterX + radius * Math.cos(a + startAngle[i]));
					let y = Math.floor(CenterY + radius * Math.sin(a + startAngle[i]));

					div.children[i].style.left = `${x}px`;
					div.children[i].style.top = `${y}px`;
				}
			}

			setInterval(function () {
				a = (a + Math.PI / 360) % (Math.PI * 2);
				moveDiv(a);
			}, 10);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите скрипт, который создает 5 "div" элементов и перемещает их по круговому пути с интервалом 100 миллисекунд.
							P.S. Можно создать любое количество div - ов).`;
	}
}

task4();