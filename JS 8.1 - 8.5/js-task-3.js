﻿function task3() {

	const body = document.querySelector('body');
	const input = document.querySelector('input');
	const btn1 = document.getElementById('btn1');

	btn1.addEventListener('click', getValueInput);

	function getValueInput(ev) {
		ev.preventDefault();

		body.style.backgroundColor = `${input.value}`;

	}
}

task3();