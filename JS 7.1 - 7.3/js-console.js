﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function clearInputData() {
	const myNode = document.getElementById("inputData");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
}

function clearConsole() {
	const myNode1 = document.querySelector("#console p");
	while (myNode1.firstChild) {
		myNode1.removeChild(myNode1.firstChild);
	}
}

function task1() {

	const btnTask1 = document.getElementById('btnTask1');

	btnTask1.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const btnExecute1 = document.createElement("button");
		const btnExecute2 = document.createElement("button");

		btnExecute1.innerHTML = 'Execute True';
		formInputData.appendChild(btnExecute1);
		btnExecute2.innerHTML = 'Execute False';
		formInputData.appendChild(btnExecute2);

		btnExecute1.addEventListener('click', executeTrue);
		btnExecute2.addEventListener('click', executeFalse);

		function executeTrue(ev) {
			ev.preventDefault();
			clearConsole();

			const point1 = createPoint(3, 3);
			const point2 = createPoint(5, 5);

			const point3 = createPoint(2, 2);
			const point4 = createPoint(4, 4);

			const point5 = createPoint(1, 1);
			const point6 = createPoint(3, 3);

			const line1 = createLine(point1, point2);
			const line2 = createLine(point3, point4);
			const line3 = createLine(point5, point6);

			function createPoint(x, y) {
				return {
					"x": x,
					"y": y
				}
			}

			function createLine(firstPoint, lastPoint) {
				return {
					firstPoint,
					lastPoint
				}
			}

			function calcDistance(x1, y1, x2, y2) {
				const distance = Math.sqrt(((x2 - x1) ** 2) + ((y2 - y1) ** 2));
				return distance;
			}

			function showMessage() {
				jsConsole.writeLine("");
				jsConsole.writeLine("Line 1:");
				jsConsole.writeLine(`Line point A(${point1.x},${point1.y})`);
				jsConsole.writeLine(`Line point B(${point2.x},${point2.y})`);
				jsConsole.writeLine("");
				jsConsole.writeLine("Line 2:");
				jsConsole.writeLine(`Line point A(${point3.x},${point3.y})`);
				jsConsole.writeLine(`Line point B(${point4.x},${point4.y})`);
				jsConsole.writeLine("");
				jsConsole.writeLine("Line 3:");
				jsConsole.writeLine(`Line point A(${point5.x},${point5.y})`);
				jsConsole.writeLine(`Line point B(${point6.x},${point6.y})`);
			}

			function canBetriangle() {
				const a = calcDistance(line1.firstPoint.x, line1.firstPoint.y, line1.lastPoint.x, line1.lastPoint.y);
				const b = calcDistance(line2.firstPoint.x, line2.firstPoint.y, line2.lastPoint.x, line2.lastPoint.y);
				const c = calcDistance(line3.firstPoint.x, line3.firstPoint.y, line3.lastPoint.x, line3.lastPoint.y);

				if (a + b > c && a + c > b && c + b > a) {
					jsConsole.writeLine("The lines CAN create triangle!");
					showMessage();
				} else {
					jsConsole.writeLine("The lines CANNOT create triangle!");
					showMessage();
				}
			}

			canBetriangle();
		}

		function executeFalse(ev) {
			ev.preventDefault();
			clearConsole();

			const point7 = createPoint(4, 4);
			const point8 = createPoint(5, 5);

			const point9 = createPoint(2, 2);
			const point10 = createPoint(4, 4);

			const point11 = createPoint(1, 1);
			const point12 = createPoint(3, 5);

			const line4 = createLine(point7, point8);
			const line5 = createLine(point9, point10);
			const line6 = createLine(point11, point12);

			function createPoint(x, y) {
				return {
					"x": x,
					"y": y
				}
			}

			function createLine(firstPoint, lastPoint) {
				return {
					firstPoint,
					lastPoint
				}
			}

			function calcDistance(x1, y1, x2, y2) {
				const distance = Math.sqrt(((x2 - x1) ** 2) + ((y2 - y1) ** 2));
				return distance;
			}

			function showMessage() {
				jsConsole.writeLine("");
				jsConsole.writeLine("Line 1:");
				jsConsole.writeLine(`Line point A(${point7.x},${point7.y})`);
				jsConsole.writeLine(`Line point B(${point8.x},${point8.y})`);
				jsConsole.writeLine("");
				jsConsole.writeLine("Line 2:");
				jsConsole.writeLine(`Line point A(${point9.x},${point9.y})`);
				jsConsole.writeLine(`Line point B(${point10.x},${point10.y})`);
				jsConsole.writeLine("");
				jsConsole.writeLine("Line 3:");
				jsConsole.writeLine(`Line point A(${point11.x},${point11.y})`);
				jsConsole.writeLine(`Line point B(${point12.x},${point12.y})`);
			}

			function canBetriangle() {
				const a = calcDistance(line4.firstPoint.x, line4.firstPoint.y, line4.lastPoint.x, line4.lastPoint.y);
				const b = calcDistance(line5.firstPoint.x, line5.firstPoint.y, line5.lastPoint.x, line5.lastPoint.y);
				const c = calcDistance(line6.firstPoint.x, line6.firstPoint.y, line6.lastPoint.x, line6.lastPoint.y);

				if (a + b > c && a + c > b && c + b > a) {
					jsConsole.writeLine("The lines CAN create triangle!");
					showMessage();
				} else {
					jsConsole.writeLine("The lines CANNOT create triangle!");
					showMessage();
				}
			}

			canBetriangle();
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функции для работы с фигурами в стандартной Плоскостной системе координат.`;
	}
}

task1();

function task2() {

	const btnTask2 = document.getElementById('btnTask2');

	btnTask2.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const btnExecute = document.createElement("button");

		btnExecute.innerHTML = 'Execute';

		formInputData.appendChild(btnExecute);

		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const array = [1, 2, 1, 4, 1, 3, 4, 1, 111, 3, 2, 1, "1"];

			Array.prototype.remove = function (elem) {
				const newArr = array.filter((v) => {
					return v !== elem;
				});

				return newArr;
			}

			jsConsole.writeLine(`Array: [${array}]`);
			jsConsole.writeLine(`New Array: [${array.remove(1)}]`);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию, которая удаляет все элементы с заданным значением.`;
	}
}

task2();

function task3() {

	const btnTask3 = document.getElementById('btnTask3');

	btnTask3.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const btnExecute = document.createElement("button");

		btnExecute.innerHTML = 'Execute';

		formInputData.appendChild(btnExecute);

		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const object = {
				one: 1,
				two: 4,
				three: "Inessa",
				four: "Ksysha"
			}

			const copyObject = Object.create(object);

			jsConsole.writeLine(copyObject.__proto__.one);
			jsConsole.writeLine(copyObject.__proto__.two);
			jsConsole.writeLine(copyObject.__proto__.three);
			jsConsole.writeLine(copyObject.__proto__.four);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию, которая делает глубокую копию объекта. Функция должна работать как для примитивных, так и для ссылочных типов.`;
	}
}

task3();