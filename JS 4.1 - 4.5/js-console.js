﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function clearInputData() {
	const myNode = document.getElementById("inputData");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
}

function clearConsole() {
	const myNode1 = document.querySelector("#console p");
	while (myNode1.firstChild) {
		myNode1.removeChild(myNode1.firstChild);
	}
}

function task1() {

	const btnTask1 = document.getElementById('btnTask1');

	btnTask1.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'String:';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '65px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			function reverseString() {
				return jsConsole.read("#input1").split('').reverse().join('');
			}

			jsConsole.writeLine(reverseString());
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Написать JavaScript функцию,  которая переворачивает строку и возвращает ее.`;
	}
}

task1();

function task2() {

	const btnTask2 = document.getElementById('btnTask2');

	btnTask2.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Expression:';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '103px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = jsConsole.read("#input1").split('');

			const newArr = [];
			const openBracketsArr = [];
			const closingBracketsArr = [];
			const noContent = [];

			arr.forEach((v, i, arr) => {
				if (v === '(' || v === ')') {
					newArr.push(v);
				}
				if (v === '(') {
					openBracketsArr.push(v);
				}
				if (v === ')') {
					closingBracketsArr.push(v);
				}
				if (arr[i] === '(' && arr[i + 1] === ')') {
					noContent.push('true');
				}
			});

			if (newArr[0] === ')' || openBracketsArr.length !== closingBracketsArr.length || noContent.length !== 0) {
				jsConsole.writeLine('Invalid expression!');
			} else if (arr.length === 0) {
				jsConsole.writeLine('Please enter a test case!');
			} else {
				jsConsole.writeLine('Valid expression!');
			}
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите JavaScript функцию, чтобы проверить, правильно ли помещены скобки в заданном выражении.`;
	}
}

task2();

function task3() {

	const btnTask3 = document.getElementById('btnTask3');

	btnTask3.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const textarea = document.createElement("textarea");
		const label2 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Text:';
		label2.innerHTML = 'Search:';
		btnExecute.innerHTML = 'Execute';

		textarea.style.marginLeft = '32px';
		btnExecute.style.marginLeft = '72px';

		formInputData.appendChild(label1);
		formInputData.appendChild(label2);
		label1.appendChild(textarea);
		label2.appendChild(input1);
		formInputData.appendChild(btnExecute);

		textarea.setAttribute('id', 'textarea');
		textarea.setAttribute('cols', '25');
		textarea.setAttribute('rows', '7');
		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		document.getElementById('textarea').value = "We are liv<b>in</b>g **in** an yellow submar<b>in</b>e. We don't have anyth<b>in</b>g else. **In**side the submar<b>in</b>e is very tight. So we are dr<b>in</b>k<b>in</b>g all the day. We will move out of it **in** 5 days.";
		document.getElementById('input1').value = 'in';

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const text = jsConsole.read("#textarea");
			const word = jsConsole.read("#input1");

			function wordCount(text, word) {

				const arr = text.toLowerCase().split('');
				const newArr = [];

				for (let i = 0; i < arr.length; i++) {
					if (arr[i] === 'i' && arr[i + 1] === 'n') {
						newArr.push('1');
					}
				}

				jsConsole.writeLine(`The substring "${word}" is contained ${newArr.length} times in the text.`);
			}

			wordCount(text, word);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Написать JavaScript функцию, которая считает, сколько раз подстрока содержится в за-данном тексте 
		(выполнить поиск без учета регистра).`;
	}
}

task3();

function task4() {

	const btnTask4 = document.getElementById('btnTask4');

	btnTask4.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Text:';
		btnExecute.innerHTML = 'Execute';

		input1.style.width = '755px';
		btnExecute.style.marginLeft = '50px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		document.getElementById('input1').value = "We are <mixcase>living</mixcase> in a <upcase>yellow submarine</upcase>. We <mixcase>don't</mixcase> have <lowcase>anything</lowcase> else.";

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const text = jsConsole.read("#input1");

			function UpCaseLowerCase(str) {
				return str.replace(/<upcase>([\s\S]*?)<\/upcase>/g,
					(elem) => elem.toUpperCase())
					.replace(/<lowcase>([\s\S]*?)<\/lowcase>/g,
						(elem) => elem.toLowerCase())
					.replace(/<mixcase>([\s\S]*?)<\/mixcase>/g,
						(elem) => [...elem].map(char => Math.floor(Math.random() * 2) ? char.toLowerCase() : char.toUpperCase()).join``);
			}

			jsConsole.writeLine(UpCaseLowerCase(text));
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Дан текст. Напишите функцию, которая изменяет текст во всех областях:
				*  в верхний регистр;
				*  в нижний регистр; 
				*  to mix casing (случайный).`;
	}
}

task4();

function task5() {

	const btnTask5 = document.getElementById('btnTask5');

	btnTask5.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const textarea = document.createElement("textarea");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Text:';
		btnExecute.innerHTML = 'Execute';

		textarea.style.marginLeft = '32px';
		btnExecute.style.marginLeft = '72px';

		formInputData.appendChild(label1);
		label1.appendChild(textarea);
		formInputData.appendChild(btnExecute);

		textarea.setAttribute('id', 'textarea');
		textarea.setAttribute('cols', '25');
		textarea.setAttribute('rows', '7');
		btnExecute.setAttribute('type', 'submit');

		document.getElementById('textarea').value = "There are some white spaces     . There are some white spaces     . There are some white spaces     . There are some white spaces     .";

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const text = jsConsole.read("#textarea");

			function spaceReplacement(text) {

				const arr = text.split('');

				const newArr = [];

				for (let i = 0; i < arr.length; i++) {
					if (arr[i] === ' ') {
						arr[i] = "&nbsp";
						newArr.push(arr[i]);
					} else {
						newArr.push(arr[i]);
					}
				}
				jsConsole.writeLine(newArr.join(''));
			}

			spaceReplacement(text);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Написать функцию, которая заменяет пробелы в тексте на <plaintext>&nbsp;`;
	}
}

task5();