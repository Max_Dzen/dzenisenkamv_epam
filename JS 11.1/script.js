﻿const divValue = document.querySelector('.value');
const divName = document.querySelector('.name');
const divResult = document.querySelector('.result');

const form = document.createElement("form");
const input = document.createElement("input");
const label = document.createElement("label");
const btn = document.createElement("button");

label.innerHTML = 'Enter your guess:';
btn.innerHTML = 'Guess number';

input.style.marginLeft = '5px';
btn.style.marginLeft = '5px';
btn.setAttribute('type', 'submit');

input.setAttribute('type', 'text');
input.setAttribute('id', 'input');

divValue.append(form);
form.append(label);
form.append(btn);
label.append(input);

let randomNumb = randomNumber();

function randomNumber() {
	const arrNumb = [];

	for (let i = 0; i < 4; i++) {
		arrNumb.push(Math.floor(Math.random() * 10))
	}

	return arrNumb;
}

randomNumber();

for (let i = 0; i < randomNumb.length; i++) {
	for (j = i + 1; j < randomNumb.length; j++) {
		if (randomNumb[i] == randomNumb[j]) {
			randomNumb[j] = randomNumb[j] + 1;
		}
	}
}

for (let i = 0; i < randomNumb.length; i++) {
	if (randomNumb[i] === 10) {
		randomNumb[i] = randomNumb[i] - 1;
	}
}

if (randomNumb[0] === 0) {
	randomNumb[0] = randomNumb[0] + 1;
}

let count = 0;

btn.addEventListener('click', getGuessNumber);

function getGuessNumber(ev) {
	ev.preventDefault();

	const inputVal = input.value.split('').map((elem) => +elem);
	let inputValBool = false;

	for (let i = 0; i < inputVal.length; i++) {
		for (let j = i + 1; j < inputVal.length; j++) {
			if (inputVal[i] == inputVal[j]) {
				inputValBool = true;
			}
		}
	}

	console.log('Random number:', randomNumb);

	count = count + 1;

	const p = document.createElement("p");
	divValue.append(p);

	let ship = 0;
	let ram = 0;

	if (inputVal.length < 4 || inputValBool || inputVal[0] === 0) {
		p.innerHTML = 'Invalid entered number. Number must contain four different digits and can not start with 0.'
	} else {
		for (let i = 0; i < 4; i++) {

			randomNumb.forEach((elem, index) => {

				if (elem === inputVal[i] && index === i) {
					ram = ram + 1;
				} else if (elem === inputVal[i]) {
					ship = ship + 1;
				}
			})
		}
		p.innerHTML = `You have: ${ram} ram(s) and ${ship} sheep(s)`;
	}

	if (ram === 4) {
		divValue.style.display = 'none';

		const form = document.createElement("form");
		const input = document.createElement("input");
		const label = document.createElement("label");
		const btn = document.createElement("button");

		label.innerHTML = 'Please enter your username:';
		btn.innerHTML = 'Submit username';

		input.style.marginLeft = '5px';
		btn.style.marginLeft = '5px';
		btn.setAttribute('type', 'submit');

		input.setAttribute('type', 'text');
		input.setAttribute('id', 'input');
		input.setAttribute('placeholder', 'Enter username');

		divName.append(form);
		form.append(label);
		form.append(btn);
		label.append(input);
		divName.prepend(p);

		p.innerHTML = "Congratulations! You have guessed the number and win the game!";

		btn.addEventListener('click', getName);

		function getName(ev) {
			ev.preventDefault();

			divName.style.display = 'none';

			const inputName = input.value;
			const p = document.createElement("p");
			divResult.append(p);

			p.innerHTML = 'Users result:';
			p.style.fontWeight = 'bold';

			localStorage[inputName] = count;

			for (let i = 0; i < localStorage.length; i++) {
				let key = localStorage.key(i);
				console.log(`${key}: ${localStorage.getItem(key)}`);

				const p = document.createElement("p");
				divResult.append(p);

				p.innerHTML = `${key}: ${localStorage.getItem(key)} moves.`;
			}
		}
	}
}

