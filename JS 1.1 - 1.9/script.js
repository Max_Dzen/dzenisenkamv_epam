function task1() {
    let x = 6;
    let y = 15;
    let z = 4;

    const result1 = document.querySelector('.result1');
    const result2 = document.querySelector('.result2');
    const result3 = document.querySelector('.result3');
    const result4 = document.querySelector('.result4');
    const result5 = document.querySelector('.result5');

    x += y - x++ * z;
    result1.innerHTML = `Результат: x = ${x}, y = ${y}, z = ${z}`;

    z = --x - y * 5;
    result2.innerHTML = `Результат: x = ${x}, y = ${y}, z = ${z}`;

    y /= x + 5 % z;
    result3.innerHTML = `Результат: x = ${x}, y = ${y}, z = ${z}`;

    z = x++ + y * 5;
    result4.innerHTML = `Результат: x = ${x}, y = ${y}, z = ${z}`;

    x = y - x++ * z;
    result5.innerHTML = `Результат: x = ${x}, y = ${y}, z = ${z}`;
}

task1();

function task2() {

    const form = document.getElementById('form1');

    form.addEventListener('submit', getResult);

    function getResult(ev) {
        ev.preventDefault();

        const x1 = document.getElementById('x1');
        const x2 = document.getElementById('x2');
        const x3 = document.getElementById('x3');

        document.getElementById('result').value = (+x1.value + +x2.value + +x3.value) / 3;

        x1.value = '';
        x2.value = '';
        x3.value = '';
    }
}

task2();

function task3() {

    const form = document.getElementById('form2');

    form.addEventListener('submit', getResult);

    function getResult(ev) {
        ev.preventDefault();

        const pi = 3.14;
        const r = document.getElementById('r');
        const h = document.getElementById('h');

        document.getElementById('V').value = pi * +r.value * 2 * +h.value;
        document.getElementById('S').value = 2 * pi * +r.value * (+r.value + +h.value);

        r.value = '';
        h.value = '';
    }
}

task3();

function task4() {

    const form = document.getElementById('form3');

    form.addEventListener('submit', getResult);

    function getResult(ev) {
        ev.preventDefault();

        const A = document.getElementById('A');
        const B = document.getElementById('B');
        let sum = 0;
        let arr = [];

        for (let i = +A.value; i <= +B.value; i++) {
            sum = sum + i;
            if (i % 2 !== 0) {
                arr.push(i);
            }
        }

        document.getElementById('sum').value = sum;
        document.getElementById('odd').value = arr;

        A.value = '';
        B.value = '';
    }
}

task4();

function task5() {

    const form = document.getElementById('form4');

    form.addEventListener('submit', getResult);

    function getResult(ev) {
        ev.preventDefault();

        const colShop = document.getElementById('colShop');

        let fact = 1;

        do {

            if (+colShop.value === 0) {
                fact = 1;
            } else {
                fact = fact * +colShop.value--;
            }

        } while (+colShop.value);

        document.getElementById('colVar').value = fact;

        colShop.value = '';
    }
}

task5();

function task6() {

    const Rectangle = document.getElementById('Rectangle');
    const RightAngledTriangle = document.getElementById('RightAngledTriangle');
    const EquilateralTriangle = document.getElementById('EquilateralTriangle');
    const Rhombus = document.getElementById('Rhombus');

    for (let i = 1; i <= 10; i++) {
        const p = document.createElement("p");
        p.innerHTML = "*".repeat(30);
        Rectangle.appendChild(p);
    }

    for (let i = 1; i <= 10; i++) {
        const p = document.createElement("p");
        p.innerHTML = "*".repeat(i);
        RightAngledTriangle.appendChild(p);
    }

    for (let i = 0; i <= 10; i++) {
        let max = 11;
        let space = "";
        let star = "";

        for (let j = 0; j < max - i; j++) space += " &nbsp";
        for (j = 0; j < 2 * i + 1; j++) star += "*";

        const p = document.createElement("p");
        p.innerHTML = space + star;
        EquilateralTriangle.appendChild(p);
    }

    for (let i = 0; i <= 10; i++) {
        let max = 11;
        let space = "";
        let star = "";

        for (let j = 0; j < max - i; j++) space += " &nbsp";
        for (j = 0; j < 2 * i + 1; j++) star += "*";

        const p = document.createElement("p");
        p.innerHTML = space + star;
        Rhombus.appendChild(p);
    }

    for (let i = 9; i >= 0; i--) {
        let max = 11;
        let space = "";
        let star = "";

        for (let j = 0; j < max - i; j++) space += " &nbsp";
        for (j = 0; j < 2 * i + 1; j++) star += "*";

        const p = document.createElement("p");
        p.innerHTML = space + star;
        Rhombus.appendChild(p);
    }
}

task6();

function task7() {

    const form = document.getElementById('form5');

    form.addEventListener('submit', getResult);

    function getResult(ev) {
        ev.preventDefault();

        const colArr = document.getElementById('colArr');

        let sum = 0;
        let arr = [];
        let arrOdd = [];

        for (let i = 0; i < +colArr.value; i++) {
            arr.push(Math.round(Math.random() * 100));
        }

        arr.sort((a, b) => {
            return a - b;
        }).forEach((v) => {
            sum = sum + v;
            if (v % 2 !== 0) {
                arrOdd.push(v);
            }
        });

        document.getElementById('createArr').value = arr;
        document.getElementById('max').value = arr[arr.length - 1];
        document.getElementById('min').value = arr[0];
        document.getElementById('sumArr').value = sum;
        document.getElementById('arithmeticMean').value = (sum / arr.length).toFixed(1);
        document.getElementById('odd7').value = arrOdd;

        colArr.value = '';
    }
}

task7();

function task8() {

    const n = 5, m = 5;
    let arr = [];

    for (let i = 0; i < m; i++) {

        arr[i] = [];

        for (var j = 0; j < n; j++) {
            arr[i][j] = Math.round(Math.random() * 10) + Math.round(Math.random() * -10);
        }
    }

    let newArr = arr.map(function func(el) {
        if (Object.prototype.toString.call(el) == "[object Array]") {
            return el.map(func);
        }
        return el;
    });

    for (let z = 0; z < n; z++) {
        newArr[z][z] = (newArr[z][z] < 0) ? 0 : 1;
        if (newArr[z][z] < 0) { newArr[z][z] = 0 };
        if (newArr[z][z] > 0) { newArr[z][z] = 1 };
    }

    const arrFirst1 = document.getElementById('arrFirst1');
    const arrFirst2 = document.getElementById('arrFirst2');
    const arrFirst3 = document.getElementById('arrFirst3');
    const arrFirst4 = document.getElementById('arrFirst4');
    const arrFirst5 = document.getElementById('arrFirst5');
    const arrLast1 = document.getElementById('arrLast1');
    const arrLast2 = document.getElementById('arrLast2');
    const arrLast3 = document.getElementById('arrLast3');
    const arrLast4 = document.getElementById('arrLast4');
    const arrLast5 = document.getElementById('arrLast5');
    arrFirst1.innerHTML = arr[0].join(' ');
    arrFirst2.innerHTML = arr[1].join(' ');
    arrFirst3.innerHTML = arr[2].join(' ');
    arrFirst4.innerHTML = arr[3].join(' ');
    arrFirst5.innerHTML = arr[4].join(' ');
    arrLast1.innerHTML = newArr[0].join(' ');
    arrLast2.innerHTML = newArr[1].join(' ');
    arrLast3.innerHTML = newArr[2].join(' ');
    arrLast4.innerHTML = newArr[3].join(' ');
    arrLast5.innerHTML = newArr[4].join(' ');
}

task8();

function task9() {

    const form = document.getElementById('form6');

    form.addEventListener('submit', getResult);

    function getResult(ev) {
        ev.preventDefault();

        const vector = document.getElementById('vector');

        let sum = 0;
        let arr = vector.value.split(' ');
        let arrB = [];

        arr.forEach((v, i) => {
            sum = sum + +v;
            arrB.push(sum / (i + 1))
        });

        document.getElementById('count').value = arrB;
        vector.value = '';
    }
}

task9();