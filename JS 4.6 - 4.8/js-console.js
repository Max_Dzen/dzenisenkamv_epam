﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function clearInputData() {
	const myNode = document.getElementById("inputData");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
}

function clearConsole() {
	const myNode1 = document.querySelector("#console p");
	while (myNode1.firstChild) {
		myNode1.removeChild(myNode1.firstChild);
	}
}

function task1() {

	const btnTask1 = document.getElementById('btnTask1');

	btnTask1.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'HTML:';
		input1.value = `<html><head><title>Sample site</title></head><body><div>text<div>more text</div>and more...</div>in body</body></html>`;
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '62px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const str = jsConsole.read("#input1").split(/<*.\w+>/).join("");

			jsConsole.writeLine(str);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию, которая извлекает содержимое html-страницы в виде текста. Функция должна возвращать все, что находится в теге, без тегов.`;
	}
}

task1();

function task2() {

	const btnTask2 = document.getElementById('btnTask2');

	btnTask2.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'URL:';
		input1.value = 'http://www.d3bg.org/forum/index.php';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '50px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr1 = jsConsole.read("#input1").split(':');
			const arr2 = jsConsole.read("#input1").split('/');
			const arr3 = jsConsole.read("#input1").split('.org');

			function ff(prot, serv, res) {
				return JSON.stringify({
					protocol: prot,
					server: serv,
					resource: res
				})
			}

			jsConsole.writeLine(ff(arr1[0], arr2[2], arr3[1]));

		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Написать скрипт, который разбирает URL-адрес.`;
	}
}

task2();

function task3() {

	const btnTask3 = document.getElementById('btnTask3');

	btnTask3.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'HTML:';
		input1.value = `<p>Please visit <a href="http://academy.telerik. com">our site</a> to choose a training course. Also visit <a href="www.devbg.org">our forum</a> to discuss the courses.</p>`;
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '62px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const text = jsConsole.read("#input1");

			jsConsole.writeLine(text.replace(`<a href="http://academy.telerik. com">our site</a>`, `[URL=http://academy.telerik. com]our site[/URL]`).replace(`<a href="www.devbg.org">our forum</a>`, `[URL=www.devbg.org]our forum[/URL]`));

		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите JavaScript функцию, заменяющую в HTML-документе, представленном в виде строки, теги.`;
	}
}

task3();