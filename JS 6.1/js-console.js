﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function clearInputData() {
	const myNode = document.getElementById("inputData");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
}

function clearConsole() {
	const myNode1 = document.querySelector("#console p");
	while (myNode1.firstChild) {
		myNode1.removeChild(myNode1.firstChild);
	}
}

function task1() {

	const btnTask1 = document.getElementById('btnTask1');

	btnTask1.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const label2 = document.createElement("label");
		const input2 = document.createElement("input");
		const label3 = document.createElement("label");
		const input3 = document.createElement("input");
		const btnIntroduce = document.createElement("button");

		label1.innerHTML = 'Enter your firstname:';
		label2.innerHTML = 'Enter your lastname:';
		label3.innerHTML = 'Enter your age:';
		btnIntroduce.innerHTML = 'Introduce';

		input2.style.marginLeft = '13px';
		input3.style.marginLeft = '57px';
		btnIntroduce.style.marginLeft = '182px';

		formInputData.appendChild(label1);
		formInputData.appendChild(label2);
		formInputData.appendChild(label3);
		label1.appendChild(input1);
		label2.appendChild(input2);
		label3.appendChild(input3);
		formInputData.appendChild(btnIntroduce);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		input2.setAttribute('type', 'text');
		input2.setAttribute('id', 'input2');
		input3.setAttribute('type', 'text');
		input3.setAttribute('id', 'input3');
		btnIntroduce.setAttribute('type', 'submit');

		btnIntroduce.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const firstName = jsConsole.read("#input1");
			const lastName = jsConsole.read("#input2");
			const age = jsConsole.read("#input3");
			const fullName = `${firstName} ${lastName}`;

			function Person(fullName, age) {
				this.fullName = fullName;
				this._age = age;
			}

			Object.defineProperties(Person.prototype, {
				'age': {
					get() {
						return this._age;
					},
					set(value) {
						this._age = value;
					}
				},
				'fullName': {
					get() {
						return `${this.firstName} ${this.lastName}`;
					},
					set(value) {
						[this.firstName, this.lastName] = value.split(" ");
					}
				},
				'introduce': {
					get() {

						if (age === "null" || age === "undefined") {
							return `Hello! My name is ${fullName} and I am 0 - years-old`;
						} else if (this.firstName.length === 0 || this.lastName.length === 0 || this.age.length === 0) {
							return `Please fill in all fields`;
						} else if (+age >= 0 && +age <= 150 && isNaN(age) === false) {

							if (this.firstName.length >= 3 && this.firstName.length <= 20 && typeof (this.firstName) === "string") {

								if (this.lastName.length >= 3 && this.lastName.length <= 20 && typeof (this.lastName) === "string") {

									const arrfirstName = this.firstName.split("");
									const arrlastName = this.lastName.split("");
									const arr = [...arrfirstName, ...arrlastName];

									function charCode(v) {
										return v.charCodeAt(0) >= 65 && v.charCodeAt(0) <= 90 || v.charCodeAt(0) >= 97 && v.charCodeAt(0) <= 122;
									}

									if (arr.every(charCode)) {
										return `Hello! My name is ${fullName} and I am ${age} - years-old`;
									} else {
										return "Incorrect name";
									}
								} else {
									return "Incorrect name";
								}
							} else {
								return "Incorrect name";
							}
						} else {
							return "Incorrect age";
						}

					}
				}
			});

			const person = new Person(fullName, age);

			jsConsole.writeLine(person.introduce);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Создайте конструктор функций для человека (Person).`;
	}
}

task1();