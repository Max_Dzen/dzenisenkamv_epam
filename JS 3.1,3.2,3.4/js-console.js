﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function clearInputData() {
	const myNode = document.getElementById("inputData");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
}

function clearConsole() {
	const myNode1 = document.querySelector("#console p");
	while (myNode1.firstChild) {
		myNode1.removeChild(myNode1.firstChild);
	}
}

function task1() {

	const btnTask1 = document.getElementById('btnTask1');

	btnTask1.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Number:';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '82px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = jsConsole.read("#input1").split('').map((elem) => +elem);
			const lastDigit = arr[arr.length - 1];

			switch (lastDigit) {
				case 0: {
					jsConsole.writeLine('zero');
					break;
				}
				case 1: {
					jsConsole.writeLine('one');
					break;
				}
				case 2: {
					jsConsole.writeLine('two');
					break;
				}
				case 3: {
					jsConsole.writeLine('three');
					break;
				}
				case 4: {
					jsConsole.writeLine('four');
					break;
				}
				case 5: {
					jsConsole.writeLine('five');
					break;
				}
				case 6: {
					jsConsole.writeLine('six');
					break;
				}
				case 7: {
					jsConsole.writeLine('seven');
					break;
				}
				case 8: {
					jsConsole.writeLine('eight');
					break;
				}
				case 9: {
					jsConsole.writeLine('nine');
					break;
				}
				default: {
					jsConsole.writeLine('error');
				}
			}
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию, которая возвращает последнюю цифру заданного целого числа в виде английского слова. 
		Примеры: 512 => "two", 1024 => "four", 12309 => "nine".`;
	}
}

task1();

function task2() {

	const btnTask2 = document.getElementById('btnTask2');

	btnTask2.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Number:';
		btnExecute.innerHTML = 'Execute';

		btnExecute.style.marginLeft = '82px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = jsConsole.read("#input1").split('').reverse().join('');

			jsConsole.writeLine(arr);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию, которая переворачивает цифры заданного десятичного числа. 
		Например: 256 => 652.`;
	}
}

task2();

function task3() {

	const btnTask3 = document.getElementById('btnTask3');

	btnTask3.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const textarea = document.createElement("textarea");
		const label2 = document.createElement("label");
		const input1 = document.createElement("input");
		const label3 = document.createElement("label");
		const input3 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Text:';
		label2.innerHTML = 'Word:';
		label3.innerHTML = 'Case sensitive:';
		btnExecute.innerHTML = 'Execute';

		textarea.style.marginLeft = '19px';
		input3.style.width = '15px';
		input3.style.height = '15px';
		btnExecute.style.marginLeft = '58px';

		formInputData.appendChild(label1);
		formInputData.appendChild(label2);
		formInputData.appendChild(label3);
		label1.appendChild(textarea);
		label2.appendChild(input1);
		label3.appendChild(input3);
		formInputData.appendChild(btnExecute);

		textarea.setAttribute('id', 'textarea');
		textarea.setAttribute('cols', '25');
		textarea.setAttribute('rows', '7');
		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		input3.setAttribute('type', 'checkbox');
		input3.setAttribute('id', 'checkbox');
		input3.setAttribute('value', 'caseSensitive');
		input3.setAttribute('checked', 'checked');
		btnExecute.setAttribute('type', 'submit');

		document.getElementById('textarea').value = "Lorem ipsum dolor Lorem sit amet consectetur adipisicing elit. Ex et assumenda suscipit, quisquam ad neque asperiores odit pariatur tenetur dignissimos quaerat voluptatem magni perspiciatis quod amet dicta nostrum eaque aut.";
		document.getElementById('input1').value = 'Lorem';

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const text = jsConsole.read("#textarea");
			const word = jsConsole.read("#input1");
			const checkbox = document.getElementById('checkbox').checked;

			function wordCount(...rest) {

				if (rest[2]) {

					const arr = rest[0].split(' ');
					const newArr = arr.filter((v) => {
						return v === rest[1];
					});

					jsConsole.writeLine(`There are ${newArr.length} occurances of the word "${rest[1]}"`);

				} else {

					const arr = rest[0].toLowerCase().split(' ');
					const newArr = arr.filter((v) => {
						return v === rest[1].toLowerCase();
					});

					jsConsole.writeLine(`There are ${newArr.length} occurances of the word "${rest[1]}"`);
				}
			}

			wordCount(text, word, checkbox);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию, которая находит все вхождения слова в текст:
		1. Поиск с учетом регистра или без учета регистра;
		2. Используйте перегрузку функций.
	`;
	}
}

task3();

function task4() {

	const btnTask4 = document.getElementById('btnTask4');

	btnTask4.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const btnExecute = document.createElement("button");

		btnExecute.innerHTML = 'Execute';
		formInputData.appendChild(btnExecute);

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const divCount = document.querySelectorAll('div');
			const arr = Array.from(divCount);

			jsConsole.writeLine(arr.length);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию для подсчета количества тегов &lt;div&gt; на веб-странице.`;
	}
}

task4();

function task5() {

	const btnTask5 = document.getElementById('btnTask5');

	btnTask5.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const label2 = document.createElement("label");
		const input1 = document.createElement("input");
		const input2 = document.createElement("input");
		const btnTest = document.createElement("button");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Sequence [, ]';
		label2.innerHTML = 'Search';
		btnTest.innerHTML = 'Test';
		btnExecute.innerHTML = 'Execute';

		input1.style.marginLeft = '35px';
		input2.style.marginLeft = '89px';
		btnTest.style.marginLeft = '145px';
		btnExecute.style.marginLeft = '145px';
		btnExecute.style.marginTop = '10px';
		btnExecute.style.display = 'block';

		formInputData.appendChild(label1);
		formInputData.appendChild(label2);
		label1.appendChild(input1);
		label2.appendChild(input2);
		formInputData.appendChild(btnTest);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		input2.setAttribute('type', 'text');
		input2.setAttribute('id', 'input2');
		btnTest.setAttribute('type', 'submit');
		btnExecute.setAttribute('type', 'submit');

		btnTest.addEventListener('click', test);
		btnExecute.addEventListener('click', execute);

		function test(ev) {
			ev.preventDefault();
			clearConsole();

			const input1 = document.getElementById('input1');
			const input2 = document.getElementById('input2');

			input1.value = '1, 3, 4, 5, 6, 7, 4, 43, 3';
			input2.value = '3';
		}

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = jsConsole.read("#input1").split(',').map((elem) => +elem);
			const elem = +jsConsole.read("#input2");

			const newArr = arr.filter((v) => {
				return v === elem;
			});

			jsConsole.writeLine(newArr.length);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию, которая подсчитывает, сколько раз встречается заданное число в заданном массиве.
		 Напишите тестовую функцию, чтобы проверить правильность работы функции.`;
	}
}

task5();

function task6() {

	const btnTask6 = document.getElementById('btnTask6');

	btnTask6.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const label2 = document.createElement("label");
		const input1 = document.createElement("input");
		const input2 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Sequence [, ]';
		label2.innerHTML = 'Position';
		btnExecute.innerHTML = 'Execute';

		input1.style.marginLeft = '35px';
		input2.style.marginLeft = '82px';
		btnExecute.style.marginLeft = '145px';

		formInputData.appendChild(label1);
		formInputData.appendChild(label2);
		label1.appendChild(input1);
		label2.appendChild(input2);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		input2.setAttribute('type', 'text');
		input2.setAttribute('id', 'input2');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = jsConsole.read("#input1").split(',').map((elem) => +elem);
			const position = +jsConsole.read("#input2");

			if (position === 0 || position === arr.length - 1) {
				jsConsole.writeLine('The position cannot be first or last element in the sequence!');
			} else if (position < 0 || position > arr.length - 1) {
				jsConsole.writeLine(`Position cannot be less than 0 or greater than ${arr.length - 1} in the sequence!`);
			} else if (arr[position - 1] < arr[position] && arr[position + 1] < arr[position]) {
				jsConsole.writeLine(true);
			} else {
				jsConsole.writeLine(false);
			}
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию, которая проверяет, является ли элемент в заданной позиции в заданном 
		массиве целых чисел большим, чем его два соседа (когда такие существуют).`;
	}
}

task6();

function task7() {

	const btnTask7 = document.getElementById('btnTask7');

	btnTask7.addEventListener('click', getResult);

	function getResult(ev) {
		ev.preventDefault();
		clearInputData();
		clearConsole();

		const formInputData = document.getElementById('inputData');
		const label1 = document.createElement("label");
		const input1 = document.createElement("input");
		const btnExecute = document.createElement("button");

		label1.innerHTML = 'Sequence [, ]';
		btnExecute.innerHTML = 'Execute';

		input1.style.marginLeft = '35px';
		btnExecute.style.marginLeft = '145px';

		formInputData.appendChild(label1);
		label1.appendChild(input1);
		formInputData.appendChild(btnExecute);

		input1.setAttribute('type', 'text');
		input1.setAttribute('id', 'input1');
		btnExecute.setAttribute('type', 'submit');

		btnExecute.addEventListener('click', execute);

		function execute(ev) {
			ev.preventDefault();
			clearConsole();

			const arr = jsConsole.read("#input1").split(',').map((elem) => +elem);

			const position = arr.findIndex(searchIndex)

			function searchIndex(elem, index, arr) {
				if (arr[index - 1] < arr[index] && arr[index + 1] < arr[index]) {
					return arr[index];
				}
			}

			const result = (position > 0) ? `Element index: ${position}` : position;

			jsConsole.writeLine(result);
		}

		const theTask = document.getElementById('theTask');
		theTask.innerHTML = `Напишите функцию, которая возвращает индекс первого элемента в массиве, который больше его соседей, 
		или -1, если нет такого элемента. Используйте функцию из предыдущего упражнения.`;
	}
}

task7();