﻿const p = document.createElement("p");
const form = document.createElement("form");
const input = document.createElement("input");
const div = document.createElement("div");

p.innerHTML = 'Don`t forget to:';

input.setAttribute('type', 'text');
input.setAttribute('id', 'input');

document.body.append(p);
document.body.append(form);

form.prepend(div);
form.append(input);

function addItem(text) {
	const p = document.createElement("p");
	const label = document.createElement("label");
	const input = document.createElement("input");
	const span = document.createElement("span");

	input.setAttribute('type', 'checkbox');
	input.setAttribute('name', 'people');

	span.innerHTML = text;

	div.append(p);
	p.append(label);
	label.append(input);
	label.append(span);
}

addItem("Ксюша");
addItem("Инесса");
addItem("Артём Артёменко");
addItem("Артём Рябов");

function appendButton(btnName, func) {

	const btn = document.createElement("button");
	btn.innerHTML = btnName;
	btn.style.marginLeft = '5px';
	btn.setAttribute('type', 'submit');
	form.append(btn);
	btn.addEventListener('click', func);

}

appendButton("Add", addPerson);
appendButton("Remove selected", removePerson);
appendButton("Hide selected", hidePerson);
appendButton("Show hidden", showPerson);

function addPerson(ev) {
	ev.preventDefault();
	const inputValue = document.getElementById('input').value;

	if (inputValue === "") {
		return;
	} else {
		addItem(inputValue);
	}

}

function removePerson(ev) {
	ev.preventDefault();

	let elem = document.querySelectorAll('input[type=checkbox]');

	for (let i = 0; i < elem.length; i++) {
		if (elem[i].checked) {
			elem[i].parentNode.parentNode.remove();
		}
	}
}

function hidePerson(ev) {
	ev.preventDefault();

	let elem = document.querySelectorAll('input[type=checkbox]');

	for (let i = 0; i < elem.length; i++) {

		if (elem[i].checked) {
			elem[i].parentNode.parentNode.style.display = "none";
		}

	}
}

function showPerson(ev) {
	ev.preventDefault();

	let elem = document.querySelectorAll('input[type=checkbox]');

	for (let i = 0; i < elem.length; i++) {
		if (elem[i].parentNode.parentNode.style.display === "none") {
			elem[i].parentNode.parentNode.style.display = "block";
		}
	}
}