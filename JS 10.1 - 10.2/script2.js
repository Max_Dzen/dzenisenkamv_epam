let currentDroppable = null;

const audio = document.querySelector("audio");
let quantity = +prompt('Какое количество мух выпустить полетать?', 50);

for (let i = 0; i < quantity; i++) {
    const img = document.createElement("img");

    img.setAttribute('src', 'imgs/муха.svg');
    img.setAttribute('alt', 'Муха');
    img.setAttribute('class', 'food');

    img.style.position = "absolute";
    img.style.top = `${Math.round(Math.random() * 500 + 250)}px`;
    img.style.left = `${Math.round(Math.random() * 1300 + 250)}px`;

    document.body.append(img);
}

const foodArr = document.querySelectorAll('.food');

Array.from(foodArr).forEach((food) => {
    food.onmousedown = function (event) {

        let shiftX = event.clientX - food.getBoundingClientRect().left;
        let shiftY = event.clientY - food.getBoundingClientRect().top;

        food.style.position = 'absolute';
        food.style.zIndex = 1000;
        document.body.append(food);

        moveAt(event.pageX, event.pageY);

        function moveAt(pageX, pageY) {
            food.style.left = pageX - shiftX + 'px';
            food.style.top = pageY - shiftY + 'px';
        }

        function onMouseMove(event) {
            moveAt(event.pageX, event.pageY);

            food.hidden = true;
            let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
            food.hidden = false;

            if (!elemBelow) return;

            let droppableBelow = elemBelow.closest('.droppable');

            if (currentDroppable != droppableBelow) {

                if (currentDroppable) {
                    leaveDroppable(currentDroppable);
                }

                currentDroppable = droppableBelow;

                if (currentDroppable) {
                    enterDroppable(currentDroppable);
                }
            }
        }

        document.addEventListener('mousemove', onMouseMove);

        food.onmouseup = function () {
            document.removeEventListener('mousemove', onMouseMove);
            food.onmouseup = null;
        };
    };

    function enterDroppable(currentDroppable) {
        currentDroppable.setAttribute('src', 'imgs/лягушкаOpen.png');
        food.addEventListener("mouseup", deleteElem);
    }

    function deleteElem(ev) {
        if (ev.screenX < 300 && ev.screenY < 300) {
            food.style.display = "none";
            audio.setAttribute('src', 'sounds/еда.mp3');
            audio.setAttribute('autoplay', 'autoplay');
            leaveDroppable(currentDroppable);
        }
    }

    function leaveDroppable(elem) {
        elem.setAttribute('src', 'imgs/лягушкаClose.png');
    }

    food.ondragstart = function () {
        return false;
    };
})


