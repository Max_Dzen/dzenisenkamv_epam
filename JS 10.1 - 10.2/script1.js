﻿function draw1() {
    const canvas = document.getElementById('canvas1');
    if (canvas.getContext) {
        const ctx = canvas.getContext("2d");

        ctx.beginPath();
        ctx.scale(1, 300 / 340); // сжимаем по вертикали
        ctx.arc(163, 193, 65, 0, Math.PI * 2, false);
        ctx.lineWidth = 3; //толщина
        ctx.strokeStyle = '#21535E';//цвет линии
        ctx.fillStyle = '#90CAD7';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        ctx.beginPath();
        ctx.scale(1, 300 / 340); // сжимаем по вертикали
        ctx.arc(123, 193, 10, 0, Math.PI * 2, false);
        ctx.stroke();

        ctx.beginPath();
        ctx.scale(1, 300 / 340); // сжимаем по вертикали
        ctx.arc(173, 217, 10, 0, Math.PI * 2, false);
        ctx.stroke();

        ctx.beginPath();
        ctx.scale(1, 340 / 100); // сжимаем по вертикали
        ctx.arc(171, 64, 2, 0, Math.PI * 2, false);
        ctx.fillStyle = '#21535E';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        ctx.beginPath();
        ctx.scale(1, 240 / 280); // сжимаем по вертикали
        ctx.arc(120, 75, 2, 0, Math.PI * 2, false);
        ctx.fillStyle = '#21535E';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(149, 75);
        ctx.lineTo(135, 89);
        ctx.lineTo(148, 89);
        ctx.lineWidth = 2; //толщина
        ctx.stroke();

        ctx.beginPath();
        ctx.ellipse(160, 59, 71, 7, 0, 0, Math.PI * 2, true);
        ctx.strokeStyle = 'black';
        ctx.fillStyle = '#396693';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke(); // обводим

        ctx.beginPath();
        ctx.ellipse(163, 19, 38, 5, 0, 0, Math.PI * 2, false);
        ctx.strokeStyle = '#262625';
        ctx.fillStyle = '#396693';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        //нижняя дуга шапки
        ctx.lineTo(201, 53);
        ctx.quadraticCurveTo(163, 66, 125, 53);
        ctx.lineTo(125, 19);
        ctx.strokeStyle = 'black';
        ctx.lineWidth = 2; //толщин
        ctx.fillStyle = '#396693';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        //улыбка человека
        ctx.beginPath();
        ctx.translate(0, 0);
        ctx.rotate(0.05); // поворачиваем координатную сетку на нужный угол
        ctx.scale(1, 100 / 580); // сжимаем по вертикали
        ctx.arc(153, 537, 25, 0, Math.PI * 2, false);
        ctx.lineWidth = 5; //толщина
        ctx.strokeStyle = '#21535E';//цвет линии
        ctx.stroke();
    }
}

draw1();

function draw2() {
    const canvas = document.getElementById('canvas2');

    if (canvas.getContext) {
        const ctx = canvas.getContext("2d");

        ctx.beginPath();
        ctx.arc(140, 170, 54, 0, Math.PI * 2, false);
        ctx.lineWidth = 2; //толщина
        ctx.strokeStyle = '#21535E';//цвет линии
        ctx.fillStyle = '#90CAD7';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(340, 170, 54, 0, Math.PI * 2, false);
        ctx.lineWidth = 2; //толщина
        ctx.strokeStyle = '#21535E';//цвет линии
        ctx.fillStyle = '#90CAD7';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(140, 170);
        ctx.lineTo(200, 100);
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(230, 166, 15, 0, Math.PI * 2, false);
        ctx.strokeStyle = '#21535E';//цвет линии
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(140, 170);
        ctx.lineTo(233, 164);
        ctx.lineTo(187, 76);
        ctx.lineTo(165, 76);
        ctx.moveTo(187, 76);
        ctx.lineTo(210, 76);
        ctx.moveTo(200, 100);
        ctx.lineTo(331, 100);
        ctx.lineTo(233, 164);
        ctx.moveTo(223, 154);
        ctx.lineTo(212, 142);
        ctx.moveTo(240, 175);
        ctx.lineTo(250, 185);
        ctx.moveTo(331, 100);
        ctx.lineTo(343, 165);
        ctx.moveTo(331, 100);
        ctx.lineTo(323, 63);
        ctx.lineTo(280, 75);
        ctx.moveTo(323, 63);
        ctx.lineTo(353, 28);
        ctx.stroke();
    }
}

draw2();

function draw3() {
    const canvas = document.getElementById('canvas3');

    if (canvas.getContext) {
        const ctx = canvas.getContext("2d");

        ctx.beginPath();
        ctx.rect(87, 168, 257, 193);
        ctx.strokeStyle = '#000000';//цвет линии
        ctx.lineWidth = 2; //толщина
        ctx.fillStyle = '#975B5B';//цвет заливки
        ctx.fill();//заливка
        ctx.lineTo(218, 23);
        ctx.lineTo(344, 168);
        ctx.fillStyle = '#975B5B';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(267, 133);
        ctx.lineTo(267, 60);
        ctx.scale(1, 300 / 940); // сжимаем по вертикали
        ctx.arc(281, 183, 14, 0, Math.PI * 2, false);
        ctx.lineTo(294, 420);
        ctx.fillStyle = '#975B5B';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        ctx.beginPath();
        ctx.rect(107, 598, 42, 88);
        ctx.rect(107, 698, 42, 88);
        ctx.rect(153, 598, 42, 88);
        ctx.rect(153, 698, 42, 88);
        ctx.fillStyle = '#000000';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        ctx.beginPath();
        ctx.rect(232, 598, 42, 88);
        ctx.rect(232, 698, 42, 88);
        ctx.rect(279, 598, 42, 88);
        ctx.rect(279, 698, 42, 88);
        ctx.fillStyle = '#000000';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        ctx.beginPath();
        ctx.rect(232, 856, 42, 88);
        ctx.rect(232, 956, 42, 88);
        ctx.rect(279, 856, 42, 88);
        ctx.rect(279, 956, 42, 88);
        ctx.fillStyle = '#000000';//цвет заливки
        ctx.fill();//заливка
        ctx.stroke();

        ctx.beginPath();
        ctx.strokeStyle = '#000000';//цвет линии
        ctx.lineWidth = 2; //толщина
        ctx.moveTo(115, 1130);
        ctx.lineTo(115, 920);
        ctx.quadraticCurveTo(150, 800, 187, 920);
        ctx.lineTo(187, 1130);
        ctx.moveTo(150, 860);
        ctx.lineTo(150, 1132);
        ctx.stroke();

        ctx.beginPath();
        ctx.ellipse(140, 1050, 4, 12, 0, 0, Math.PI * 2, false);
        ctx.stroke();

        ctx.beginPath();
        ctx.ellipse(160, 1050, 4, 12, 0, 0, Math.PI * 2, false);
        ctx.stroke();
    }
}

draw3();